provider "aws" {
  region = var.region
}

resource "aws_instance" "gitlab" {
  ami = var.ami
  instance_type = var.instance_type
  key_name = var.key_name
  tags = {
    Name = "gitlab"
  }
}

resource "aws_acm_certificate" "gitlab" {
  domain_name = var.domain_name
  validation_method = "DNS"
}

resource "aws_acm_certificate_validation" "gitlab" {
  certificate_arn = aws_acm_certificate.gitlab.arn
  domain_name = aws_acm_certificate.gitlab.domain_name
}

resource "aws_route53_record" "gitlab_a" {
  name = var.domain_name
  type = "A"
  ttl = var.ttl
  hosted_zone_id = var.hosted_zone_id
  records = [aws_instance.gitlab.public_ip]
}

resource "aws_route53_record" "gitlab_cname" {
  name = "*.${var.domain_name}"
  type = "CNAME"
  ttl = var.ttl
  hosted_zone_id = var.hosted_zone_id
  cname = aws_acm_certificate.gitlab.domain_validated_certificate_arn
}

resource "local_file" "gitlab_ssl" {
  content = base64decode(aws_acm_certificate.gitlab.certificate_pem)
  filename = "/etc/gitlab/ssl/gitlab.pem"
}

resource "local_file" "gitlab_key" {
  content = base64decode(aws_acm_certificate.gitlab.private_key)
  filename = "/etc/gitlab/ssl/gitlab.key"
}
